// Reorganize the PAL-ARGO data on an SFTP server
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strings"

	"github.com/pkg/errors"
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

var usage = `Usage: dataorg [options]

Organize the incoming data on the PAL-ARGO SFTP server. All of the incoming
data is stored in the top-level data directory ($DATA). This program moves
each file into $DATA/$ID, where $ID is the ARGO float ID number.

`

var (
	dataDir     = flag.String("dir", "data", "top-level data directory")
	sftpUser    = flag.String("user", "pal-argo", "SFTP user account")
	sftpKeyfile = flag.String("keyfile", "", "SSH private key to use")
	sftpHost    = flag.String("host", "96.127.80.236:22", "SFTP server host")
	credFile    = flag.String("creds", "", "Credentials file with username:password")
)

func pubkeyConnect(user *url.Userinfo, host, keyFile string) (*ssh.Client, error) {
	config := &ssh.ClientConfig{
		User: user.Username(),
		Auth: []ssh.AuthMethod{},
		// Host authentication is not needed for this application
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	// Add public-key authentication is key file is provided
	if keyFile != "" {
		key, err := ioutil.ReadFile(keyFile)
		if err != nil {
			return nil, fmt.Errorf("read %q: %w", keyFile, err)
		}

		signer, err := ssh.ParsePrivateKey(key)
		if err != nil {
			return nil, fmt.Errorf("parse %q: %w", keyFile, err)
		}
		config.Auth = append(config.Auth, ssh.PublicKeys(signer))
	}

	// Add password based authentication if password is provided
	if pword, ok := user.Password(); ok {
		config.Auth = append(config.Auth, ssh.Password(pword))
	}

	if len(config.Auth) == 0 {
		return nil, errors.New("No authentication method provided")
	}

	return ssh.Dial("tcp", host, config)
}

func organizeFiles(conn *sftp.Client, topdir string) error {
	info, err := conn.ReadDir(topdir)
	if err != nil {
		return err
	}

	listing := make(map[string]os.FileInfo)
	for _, fi := range info {
		listing[fi.Name()] = fi
	}

	var dummy os.FileInfo
	for name, fi := range listing {

		// Don't process directories or hidden files
		if fi == nil || fi.IsDir() || name[0] == '.' {
			continue
		}

		// Filename format: $ID.NNN.$EXT
		parts := strings.Split(name, ".")
		if len(parts) == 1 {
			continue
		}

		// ID number becomes the subdirectory
		dirname := parts[0]
		// Create directory unless it already exists
		if _, found := listing[dirname]; !found {
			newdir := conn.Join(topdir, dirname)
			if err := conn.Mkdir(newdir); err != nil {
				log.Printf("Cannot create directory: %q", newdir)
				return err
			} else {
				// Add a dummy entry for the newly created directory so
				// we don't try to create it again.
				listing[dirname] = dummy
				log.Printf("mkdir: %q", newdir)
			}
		}

		// Rename the file
		oldname := conn.Join(topdir, name)
		newname := conn.Join(topdir, dirname, name)
		err := conn.PosixRename(oldname, newname)
		if err != nil {
			_, err = conn.Stat(newname)
			if err != nil {
				log.Printf("Rename %q -> %q failed (%v)", oldname, newname, err)
			}
			continue
		} else {
			log.Printf("Rename: %q -> %q", oldname, newname)
		}
	}

	return nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()

	var ui *url.Userinfo
	if *credFile != "" {
		contents, err := os.ReadFile(*credFile)
		if err != nil {
			log.Fatalf("read %q: %v", *credFile, err)
		}
		fields := strings.Split(strings.TrimSpace(string(contents)), ":")
		if len(fields) < 2 {
			log.Fatalf("Invalid credentials: %q", contents)
		}
		ui = url.UserPassword(fields[0], fields[1])
	} else {
		ui = url.User(*sftpUser)
	}

	sshconn, err := pubkeyConnect(ui, *sftpHost, *sftpKeyfile)
	if err != nil {
		log.Fatal(err)
	}
	defer sshconn.Close()

	conn, err := sftp.NewClient(sshconn)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	if err := organizeFiles(conn, *dataDir); err != nil {
		log.Fatal(err)
	}
}
