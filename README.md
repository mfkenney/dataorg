# PAL-ARGO Data Organizer

Organize PAL-ARGO data on an SFTP server. This program reads all files in the top-level data directory, $DATA, and moves them to a float-specific subdirectory, `$DATA/$ID`, where `$ID`  is the ARGO float ID number.

## Installation

Download the OS and architecture specific archive from the Downloads section of this repository, unpack and install the executable file somewhere in your $PATH.

## Usage

You will need to specify a username (`-user=NAME`) and an SSH public key file (`-keyfile=FILENAME`) or a credentials file containing *username:password* (`-creds=FILENAME`).

``` shellsession
$ dataorg --help
Usage: dataorg [options]

Organize the incoming data on the PAL-ARGO SFTP server. All of the incoming
data is stored in the top-level data directory ($DATA). This program moves
each file into $DATA/$ID, where $ID is the ARGO float ID number.

  -creds string
    	Credentials file with username:password
  -dir string
    	top-level data directory (default "data")
  -host string
    	SFTP server host (default "96.127.80.236:22")
  -keyfile string
    	SSH private key to use
  -user string
    	SFTP user account (default "pal-argo")
```

Run the program from a `cron` job (or similar) to keep the data server organized.
