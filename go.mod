module bitbucket.org/mfkenney/dataorg

go 1.15

require (
	github.com/pkg/errors v0.8.0
	github.com/pkg/sftp v1.13.5
	golang.org/x/crypto v0.3.0
)
